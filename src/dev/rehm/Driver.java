package dev.rehm;

import dev.rehm.models.Bread;
import dev.rehm.models.SlicedBread;

public class Driver {
	
	public static void main(String[] args) {
		/* working with a bread class that initially had three instance variables 
//		Bread bread1 = new Bread(false, "Whole wheat", true);
		Bread bread2 = new Bread(); // because we didn't initialize any values for this object we were given default values - default values are only provided in instance and static scope
//		System.out.println(bread1.toString());
		System.out.println(bread2.toString());
		*/
		
		// refactored Bread class
		/*
		Bread bread1 = new Bread("Whole Grain");
//		bread1.type = "White";
		System.out.println(bread1);
		bread1.setType("White");
		bread1.setType(null);
		System.out.println(bread1);
		*/
		
		//taking a look at our sliced bread class
		//dev.rehm.models.SlicedBread slicedBread = new dev.rehm.models.SlicedBread(); // another way to access Classes out of the package

//		SlicedBread slicedBread1 = new SlicedBread();
//		SlicedBread slicedBread2 = new SlicedBread(true,"cream cheese","cinnamon");
//		System.out.println(slicedBread1);
//		System.out.println(slicedBread2);

		
		// taking a look at constructors:
		/*
		Bread b = new Bread(); // the only time we are given a default constructor is when we don't include one in the class
		System.out.print(b.toString());
//		Bread b2 = new Bread(34);
		 * 
		 */
		
		// taking a look at the equals method - before overriding it, it has the same implementation as the "==" http://hg.openjdk.java.net/jdk8/jdk8/jdk/file/687fd7c7986d/src/share/classes/java/lang/Object.java#l148
		Bread b1 = new Bread();
		Bread b2 = new Bread();
		System.out.println(b1);
		System.out.println(b2);
		System.out.println("==?"+ (b1==b2));
		System.out.println(".equals?" + b1.equals(b2));
		
		// another example of polymorphism takes the form of covariance - Bread and Sliced bread are covariant types
		// you can refer to any child class as an instance of its superclass
		Bread b3 = new SlicedBread();
		System.out.println(b3.toString()); // this is virtual method invocation - the most specific implementation is invoked 
//		SlicedBread b4 = new Bread();
		SlicedBread b4 = (SlicedBread) b3;
		b4.getSpread();
		b3.getType();
		
		
	}

}
