package dev.rehm;

import dev.rehm.models.Order;
import dev.rehm.services.IoService;
import dev.rehm.services.OrderService;

public class OrderDriver {

	public static void main(String[] args) {
		OrderService orderService = new OrderService();
		Order result = orderService.takeOrder();
		System.out.println(result); 
		// write the order to a file
		IoService ioService = new IoService();
		ioService.writeOrderToFile(result);
	}
	
}
