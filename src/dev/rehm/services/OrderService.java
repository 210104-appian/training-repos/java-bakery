package dev.rehm.services;

import java.util.ArrayList;
import java.util.Scanner;

import dev.rehm.models.BakeryItem;
import dev.rehm.models.Bread;
import dev.rehm.models.Coffee;
import dev.rehm.models.Muffin;
import dev.rehm.models.Order;
import dev.rehm.models.OrderStatus;
import dev.rehm.models.Size;

public class OrderService {
	
	Scanner scanner = new Scanner(System.in);
	
	public Order takeOrder() {
		Order order = new Order(OrderStatus.PENDING);
		String orderName = takeOrderName();
		order.setCustomerName(orderName);
		String orderPhone = takeOrderPhoneNumber();
		order.setPhone(orderPhone);
		
//		BakeryItem item = takeOrderItem();
//		order.setItem(new BakeryItem[] {item});
		
		/*int orderQuantity = takeOrderQuantity();
		BakeryItem[] bakeryItems = new BakeryItem[orderQuantity];
		for(int i=0; i<orderQuantity; i++) {
			bakeryItems[i] = takeOrderItem();
		}
		System.out.println(order);
		// bakeryItems array contains the selected items
		order.setItem(bakeryItems);
		*/
		//take order for items 
		ArrayList<BakeryItem> items = new ArrayList<>();
		String selection;
		do {
		BakeryItem item = takeOrderItem();
		items.add(item); 
		System.out.println("Would you like to order another item? (Y/N)");
		selection = scanner.next();
		} while (selection.toUpperCase().charAt(0)=='Y');
		order.setItems(items);
		return order;
	}
	
	private int takeOrderQuantity() {
		System.out.println("Thank you! How many items would you like to order?");
		int numOfItems = scanner.nextInt(); //this risks a runtime exception, we should take this in as a string and check it against a regular expression and then parse it manually 
		return numOfItems;
	}
	
	private String takeOrderName() {
		System.out.println("Can we get a name for your order?");
		String name = scanner.next();
		return name;
	}
	
	private String takeOrderPhoneNumber() {
		System.out.println("Please enter phone number:");
		String phone = scanner.next();
		return phone;
	}

	
	private BakeryItem takeOrderItem() {
		System.out.println("Would you like to order (1) coffee, (2) muffin, or (3) bread?");
		String selection = scanner.next();
		switch(selection) {
		case "1":
			return takeCoffeeOrder();
		case "2":
			return takeMuffinOrder();
		case "3":
			return takeBreadOrder();
		default:
			System.out.println("Invalid input. Please try again.");
			return takeOrderItem();
		}
	}
	
	private Muffin takeMuffinOrder() {
		System.out.println("What kind of muffin would you like to order?");
		String flavor = scanner.next();
		return new Muffin(3.00, flavor);
	}
	
	private Bread takeBreadOrder() {
		System.out.println("What kind of bread would you like to order?");
		String type = scanner.next();
		return new Bread(type, 4.00);
	}
	
	private Coffee takeCoffeeOrder() {
		Size coffeeSize = getCoffeeSize();
		double price = 2.00;
		if(coffeeSize==Size.M) {
			price = 2.50;
		} else if (coffeeSize==Size.L) {
			price = 3.00;
		}
		String coffeeRoast = getCoffeeRoast();
		return new Coffee(coffeeSize, coffeeRoast, price);
	}
	
	private Size getCoffeeSize() {
		System.out.println("What size coffee would you like? (S, M, L)");
		String selection = scanner.next();
		if(selection.equals("S") || selection.equals("M") || selection.equals("L")) {
			return Size.valueOf(selection);
		}
		System.out.println("Invalid input. Please try again.");
		return getCoffeeSize();
	}
	
	private String getCoffeeRoast() {
		System.out.println("Would you like (1) light roast, (2) medium roast, or (3) dark roast?");
		String selection = scanner.next();
		switch(selection) {
		case "1":
			return "Light Roast";
		case "2":
			return "Medium Roast";
		case "3":
			return "Dark Roast";
		default:
			return getCoffeeRoast();
		}
	}
	
	
}
