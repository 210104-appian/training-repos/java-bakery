package dev.rehm.services;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import dev.rehm.models.Order;

public class IoService {

	
	public void writeOrderToFile(Order order) {
		/* without using try with resources
		try {
			//checked exception - we need to wrap this in a try catch or add a throws clause to get this to compile
			FileWriter fw = new FileWriter("src/dev/rehm/orders.txt");
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(order.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// here is where I would want to close my resources
		}
		*/
		
		try (FileWriter fw = new FileWriter("src/dev/rehm/orders.txt", true);  // optional boolean param determines whether we overwrite/append to the file 
			BufferedWriter bw = new BufferedWriter(fw);){
			bw.write("\n"+order.toString());
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	
}
