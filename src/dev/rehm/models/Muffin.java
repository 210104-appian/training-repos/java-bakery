package dev.rehm.models;

public class Muffin extends BakeryItem {

	private String flavor;
	
	public Muffin() {
		super();
	}
	
	public Muffin(double price, String flavor) {
		super(price);
		this.flavor = flavor;
	}

	public String getFlavor() {
		return flavor;
	}

	public void setFlavor(String flavor) {
		this.flavor = flavor;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((flavor == null) ? 0 : flavor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Muffin other = (Muffin) obj;
		if (flavor == null) {
			if (other.flavor != null)
				return false;
		} else if (!flavor.equals(other.flavor))
			return false;
		return true;
	}

	@Override
	public void purchaseItem() {
		System.out.println("purchasing muffin for $"+price);
		
	}

	@Override
	public String toString() {
		return "Muffin [flavor=" + flavor + ", price=" + price + "]";
	}
	
	
	
}
