package dev.rehm;

import java.util.Arrays;

import dev.rehm.models.BakeryItem;
import dev.rehm.models.Bread;
import dev.rehm.models.Coffee;
import dev.rehm.models.Muffin;
import dev.rehm.models.Size;

public class AbstractionDriver {

	public static void main(String... args) {
		/*
		Coffee c1 = new Coffee(Size.SMALL, "Light", 2.00);
		System.out.println(c1);
		
//		BakeryItem c2 = new BakeryItem(); // cannot instantiate an abstract class directly
		BakeryItem b1 = new Bread();
		BakeryItem b2 = new Coffee();
		b1.purchaseItem();
		b2.purchaseItem();
		*/
		
		
		BakeryItem[] bakeryItems = new BakeryItem[5]; // allocating 5 places in memory for 5 bakery items
		bakeryItems[0] = new Coffee(Size.M, "Dark", 2.50);
		bakeryItems[1] = new Bread("Wheat",4.00);
		bakeryItems[2] = new Muffin(3.00,"Blueberry");
		bakeryItems[3] = new Muffin(3.00,"Lemon Poppyseed");
		bakeryItems[4] = new Muffin(3.00,"Lemon Poppyseed");
		bakeryItems[4] = new Muffin(3.00, "Bran");
		System.out.println(bakeryItems);
//		Arrays.sort(bakeryItems); // we have not determined comparison metric - we would need to do this using Comparable or Comparator
		System.out.println(Arrays.toString(bakeryItems));
		
		
		for(int i=0;i<bakeryItems.length;i++) {
			System.out.println(bakeryItems[i]);
		}
		
		for(BakeryItem item : bakeryItems) {
			System.out.println(item);
		}
		
		double sub = determineSubtotal(bakeryItems);
		System.out.println("subtotal for your items comes out to be: "+sub);
		
		// null pointer exception - we fixed this by comparing the string literal to the flavor rather than the other way around
		Muffin muffin = new Muffin();
		System.out.println(muffin);
		if("blueberry".equals(muffin.getFlavor())) { //if (muffin.getFlavor()!=null && muffin.getFlavor().equals("blueberry"))
			System.out.println("Yum! I love blueberry muffins!");
		} else {
			System.out.println("That muffin would be better with blueberries");
		}
		
		//System.out.println(bakeryItems[7]); // this caused an arrayindexoutofboundexecption 
//		throw new RuntimeException(); // this is a runtime exception and therefore unchecked
		
//		throw new Exception();
	}

	
	public void purchaseBakeryItem(BakeryItem bakeryItem) {
		bakeryItem.purchaseItem();
	}
	
	public static double determineSubtotal(BakeryItem[] items) {
		double subtotal = 0.0;
		for(int i=0; i<items.length;i++) {
			subtotal = subtotal + items[i].getPrice();
		}
		return subtotal;
	}
}
